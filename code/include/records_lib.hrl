%% @author Sergio Gil Luque
%% @version 0.1
%% @title Fichero de cabeceras
%% @doc Contiene cabeceras de registros para ser usados por los modulos

-record(gps_point,{index,rcr,date,time,valid,latitude,n_s,longitude,e_w,height,speed,heading,hdop,distance}).
-record(gps_operations, {index, lat, long, date, time, timeStamp, speedGPS, speedCalc, acceleration, a_tan, a_rad, inclination, timeCalc, headingGPS, headingCalc, headingDiff, hdop, distanceGPS, distanceCalc, radius}).
