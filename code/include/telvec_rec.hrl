%% Copyright © 2013 Sergio Gil Luque, Ángel Herranz
%% All rights reserved

-include("trackp_rec.hrl").

-record(telvec,
        {
          p :: [trackps:track()],
          a :: vectors:vector(), % Acceleration
          % Derived data from acceleration
          a_r :: float(),
          a_t :: float(),
          lean :: float()
        }).
