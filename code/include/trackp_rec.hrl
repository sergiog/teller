%% Copyright © 2013 Sergio Gil Luque, Ángel Herranz
%% All rights reserved

-record(trackp,
        {
          index :: integer(),
          latt :: float(),
          long :: float(),
          height :: float(),
          dist :: float(),
          speed :: float(),
          hdop :: float()
        }).
