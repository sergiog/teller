%% Copyright © 2013 Sergio Gil Luque, Ángel Herranz
%% All rights reserved

%% @author Sergio Gil Luque
%% @version 0.46
%% @title Telemetria con GPS - Modulo mathsGPS
%% @doc Mathematical operations using GPS data 


-module(mathsGPS).

%% @headerfile records_lib.hrl
-include("../include/records_lib.hrl").

-export([begin_calculations/1]).

%% @type Gps_point = #gps_point{index = integer(),rcr=,date=,time=,valid=,latitude=float(),n_s= N | S,longitude = float(),e_w = E|W,height= float(),speed = float(),heading,hdop,distance = float()}


%% @spec begin_calculations( [string()]) -> {error, string()} | List
%%	List = [float()]
begin_calculations([_,P1|L]) when is_record(P1, gps_point)->
	Record_heading = #gps_operations{
	  index="INDEX",
	  date="DATE", 
	  time="TIME", 
	  timeStamp = "TIME_STAMP",
	  speedGPS="SPEED_FROM_GPS", 
	  speedCalc="SPEED_CALCULATED", 
	  acceleration = "ACCELERATION",
	  a_tan = "A_TAN",
	  a_rad = "A_RAD",
	  inclination= "INCLINATION", 
	  timeCalc = "FREQUENCY_CALCULATED", 
	  headingGPS="HEADING_FROM_GPS", 
	  headingCalc="HEADING_CALCULATED",
	  headingDiff = "HEADING_DIFF", 
	  hdop="HDOP", 
	  distanceGPS="DISTANCE_FROM_GPS", 
	  distanceCalc="DISTANCE_CALCULATED",
	  radius="RADIUS"},

	First_point = #gps_operations{
	  index=P1#gps_point.index,
	  lat=P1#gps_point.latitude, 
	  long=P1#gps_point.longitude , 
	  date=P1#gps_point.date,
	  time=P1#gps_point.time,
	  timeStamp = getTimeStamp (P1#gps_point.date, P1#gps_point.time),
	  speedGPS=P1#gps_point.speed,
	  speedCalc=0, 
	  acceleration = 0,
	  a_tan = 0,
	  a_rad = 0,
	  inclination = null, 
	  timeCalc = null,
	  headingGPS=P1#gps_point.heading,
	  headingCalc=0,
	  hdop=P1#gps_point.hdop,
	  distanceGPS=P1#gps_point.distance,
	  distanceCalc=0.0},

    [Heading|Calc_1] = [Record_heading, First_point | process_points_1([P1|L])],
    [Heading|process_points_2 (Calc_1)].


getAcceleration (P1,P2) ->
    SpeedDiff = (P2#gps_operations.speedGPS - P1#gps_operations.speedGPS) / 3.6,
    TimeDiff = (P2#gps_operations.timeStamp - P1#gps_operations.timeStamp) / 1000,
    {A_tot, A_tan, A_rad} = get_acc_components(P1,P2),
    %{SpeedDiff / TimeDiff, A_tan,A_rad}.
    {A_tot,A_tan,A_rad}.

get_acc_components(P1,P2) ->
    P1_heading = P1#gps_operations.headingCalc,
    P1_speed = (P1#gps_operations.speedGPS/3.6),
    P2_heading = P2#gps_operations.headingCalc,
    P2_speed = (P2#gps_operations.speedGPS/3.6),
    V1 = vectors:polar(vectors:head_to_a(P1_heading),P1_speed),
    %DegAngle = vectors:to_deg(Angle),
    V2 = vectors:polar(vectors:head_to_a(P2_heading),P2_speed),
    AVr = vectors:dif(vectors:rot(V2,-vectors:a(V1)),
                      vectors:rot(V1,-vectors:a(V1))),
    A_tot2 = vectors:r(AVr)/0.2,

    A_tan = vectors:x(AVr)/0.2,
    A_rad = vectors:y(AVr)/0.2,
    A_tot = math:sqrt((math:pow(A_tan, 2) + math:pow(A_rad, 2))),
    {A_tot2,A_tan, A_rad}.
    %{A_tan,A_rad,A_tot2}.
    
process_points_2 ([]) ->
    [];

process_points_2 ([P1]) ->   
    [P1];

process_points_2 ([P1,P2]) ->
    {Acceleration,A_tan,A_rad} = getAcceleration(P1,P2),
    ResP2 = P2#gps_operations{acceleration = Acceleration,
			      a_tan = A_tan,
			      a_rad = A_rad},
    [P1,ResP2];

process_points_2 ([P1,P2,P3])  when is_record (P1, gps_operations), is_record (P2, gps_operations), is_record (P3, gps_operations) ->
    {Acceleration, A_tan, A_rad} = getAcceleration(P1,P2),
    %AccelerationP3 = getAcceleration(P2,P3),
    ResP2 = P2#gps_operations{radius = getRadius(P2,P3),
			      headingDiff=0, 
			      acceleration = Acceleration,
			      a_tan = A_tan,
			      a_rad = A_rad},

    ResP3 = P3#gps_operations{headingDiff = abs (P3#gps_operations.headingCalc - P2#gps_operations.headingCalc)},
    [P1,ResP2,ResP3];

process_points_2 ([P1,P2,P3|T])  when is_record (P1, gps_operations), is_record (P2, gps_operations), is_record (P3, gps_operations) ->
    {Acceleration,A_tan,A_rad}  = getAcceleration(P1,P2),
    ResP2=P2#gps_operations{radius = getRadius(P2,P3),
			    headingDiff=0,
			    acceleration = Acceleration,
			   a_tan = A_tan,
			   a_rad = A_rad},
    [P1,ResP2| proc_2_bucle([ResP2,P3|T])].



proc_2_bucle ([P1,P2,P3]) when is_record (P1, gps_operations), is_record (P2, gps_operations), is_record (P3, gps_operations) ->
    {AccelerationP2,AtanP2,AradP2} = getAcceleration(P1,P2),
    {AccelerationP3, AtanP3, AradP3}= getAcceleration(P2,P3),
    ResP2=P2#gps_operations{
	    radius = getRadius(P2,P3),
	    headingDiff = abs (P2#gps_operations.headingCalc - P1#gps_operations.headingCalc), 
	    acceleration = AccelerationP2,
	    a_tan = AtanP2,
	    a_rad = AradP2},
    ResP3 = P3#gps_operations{headingDiff = P3#gps_operations.headingCalc - P2#gps_operations.headingCalc, 
			      acceleration = AccelerationP3,
			     a_tan= AtanP3,
			     a_rad =AradP3},
    [ResP2,ResP3];

proc_2_bucle ([P1,P2,P3|T])  when is_record (P1, gps_operations), is_record (P2, gps_operations), is_record (P3, gps_operations) ->
    {Acceleration,Atan,Arad} = getAcceleration(P1,P2),
    ResP2=P2#gps_operations{
	    radius = getRadius(P2,P3),
	    headingDiff = abs (P2#gps_operations.headingCalc - P1#gps_operations.headingCalc),
	    acceleration = Acceleration,
	   a_tan = Atan,
	   a_rad = Arad},

    [ResP2 | proc_2_bucle ([ResP2,P3|T])].



getRadius(P2,P3) when is_record (P2, gps_operations), is_record (P3,gps_operations)->
    D1 =  P2#gps_operations.distanceCalc,
    D3 =  P3#gps_operations.distanceCalc,
    A1 = toRadians(P2#gps_operations.headingCalc),
    A2 = toRadians(P3#gps_operations.headingCalc),
    P1_X = -D1 * math:cos(A1),
    P1_Y = -D1 * math:sin(A1),
    P3_X = D3 * math:cos(A2),
    P3_Y = D3 * math:sin(A2),
    D2 = math:sqrt(math:pow(P1_X - P3_X,2) + math:pow(P1_Y - P3_Y,2)),
    getRadius_2(D1,D2,D3).


%% http://mathforum.org/dr.math/faq/formulas/faq.triangle.html
getRadius_2(D1,D2,D3) ->
    S = (D1+D2+D3)/2,
    Area = math:sqrt(S*(S-D1)*(S-D2)*(S-D3)),
    (D1*D2*D3) /(4 * Area).



%% @spec getTimeStamp ( date() ) -> integer()
getTimeStamp (Date, {H,M,S,Mm}) ->
    Time = {H,M,S},
    calendar:datetime_to_gregorian_seconds({Date, Time}) * 1000 + Mm.


getBearing(P1,P2) when is_record(P1,gps_point), is_record(P2, gps_point)->
    Lat1 = toRadians(P1#gps_point.latitude),
    Lat2 = toRadians(P2#gps_point.latitude),
    Long1 = toRadians(P1#gps_point.longitude),
    Long2 = toRadians(P2#gps_point.longitude),
    Dlong = Long2 - Long1,

    P = math:atan2(math:sin(Dlong)*math:cos(Lat2), math:cos(Lat1)* math:sin (Lat2) - math:sin(Lat1) * math:cos(Lat2) * math:cos(Dlong)),
    toPositive (toDegrees(P)).
    %toDegrees(P).

toPositive (D) when is_float(D) ->
    if 
	D >= 0 ->
	    D;
	D < 0 ->
	    D + 360
    end.


%% @spec distances *******
process_points_1([]) -> [];
process_points_1([_]) -> [];
%%distances([P1,P2|T]) -> [{distancia, io:format("~.3f ~n" ,[haverSine(P1,P2)])} | distances([P2|T])].
process_points_1([P1,P2|T]) when is_record (P1, gps_point), is_record (P2, gps_point) -> 
    DistCalc = haverSine(P1,P2),
    Frec_diff =  getTimeStamp (P2#gps_point.date, P2#gps_point.time) -  getTimeStamp (P1#gps_point.date, P1#gps_point.time),
    Speed = speed(DistCalc,Frec_diff ),
    Heading = getBearing(P1,P2),
	[#gps_operations{index=P2#gps_point.index,
			 date=P2#gps_point.date,
			 lat=P2#gps_point.latitude,
			 long=P2#gps_point.longitude,
			 time=P2#gps_point.time,
			 timeStamp = getTimeStamp (P2#gps_point.date, P2#gps_point.time),
			 speedGPS=P2#gps_point.speed,
			 speedCalc=Speed,
			 timeCalc = (DistCalc * 3600) / P2#gps_point.speed,
			 headingGPS=P2#gps_point.heading,
			 headingCalc=Heading,
			 hdop=P2#gps_point.hdop,
			 distanceGPS=P2#gps_point.distance,distanceCalc=DistCalc} | process_points_1 ([P2|T])
	].


%% @spec speed (float()) -> float()
speed(Distance,Frec) when is_float (Distance), is_integer(Frec) ->
    (Distance / Frec) * 3600.


%% @spec toRadians (float()) -> float()
toRadians (X) when is_float(X) ->
	(X * math:pi()) /180.


toDegrees (X) when is_float(X) ->
    (X * 180) / math:pi().


%% @spec haverSine (Gps_point, Gps_point) -> float() 
haverSine (P1, P2) when is_record (P1, gps_point), is_record (P2, gps_point) ->
	Lat1 = toRadians(P1#gps_point.latitude),
	Long1 = toRadians(P1#gps_point.longitude),
	Lat2 = toRadians(P2#gps_point.latitude),
	%io:format("~.3f ~n", [Lat2] ),
	Long2 = toRadians(P2#gps_point.longitude),
	R = 6372797.560856, % Radio de la Tierra

	2 * R * math:asin(math:sqrt(math:pow(math:sin((Lat2 - Lat1) / 2), 2) + math:cos(Lat1) * math:cos(Lat2) * math:pow(math:sin((Long2 - Long1) / 2) ,2))).




