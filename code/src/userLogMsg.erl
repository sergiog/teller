%% Copyright © 2013 Sergio Gil Luque, Ángel Herranz
%% All rights reserved

%% @author Sergio Gil Luque
%% @version 0.30
%% @title Telemetria con GPS - Modulo userLogMsg
%% @doc Creates a log with useful information (for debug)


-module(userLogMsg).

%% @headerfile records_lib.hrl
-include("../include/records_lib.hrl").

-export([begin_recopilation/2]).

begin_recopilation([_|T], Fd) ->
    recopilation_bucle (T, Fd).


recopilation_bucle([H|T], Fd) ->
    get_info(H, Fd),
    recopilation_bucle(T, Fd);

recopilation_bucle(X, Fd) ->
    get_info(X, Fd).

get_info(Point, Fd) when is_record (Point, gps_operations) ->
    %io:fwrite("Point is: ~p~n", [Point]),
    Index = Point#gps_operations.index,
    Speed = Point#gps_operations.speedGPS,
    Acceleration = Point#gps_operations.acceleration,
    Atan = Point#gps_operations.a_tan,
    Arad = Point#gps_operations.a_rad,
    Distance = Point#gps_operations.distanceCalc,
    HeadingGPS =  Point#gps_operations.headingGPS,
    HeadingCalc = Point#gps_operations.headingCalc,
    Radius = Point#gps_operations.radius,


    ok = file:write (Fd, io_lib:fwrite("~nPOINT ~B ~n", [Index])),
    ok = file:write (Fd, io_lib:fwrite("      SPEED: ~p ~n", [Speed])),
    ok = file:write (Fd, io_lib:fwrite("      ACCELERATION: ~p ~n", [Acceleration])),
    ok = file:write (Fd, io_lib:fwrite("      A_TAN: ~p ~n", [Atan])),
    ok = file:write (Fd, io_lib:fwrite("      A_RAD: ~p ~n", [Arad])),
    ok = file:write (Fd, io_lib:fwrite("      DISTANCE FROM LAST POINT: ~p ~n", [Distance])),
    ok = file:write (Fd, io_lib:fwrite("      HEADING GPS: ~p ~n", [HeadingGPS])),
    ok = file:write (Fd, io_lib:fwrite("      HEADING CALCULATED: ~p ~n", [HeadingCalc])),
    ok = file:write (Fd, io_lib:fwrite("      RADIUS USING LAST AND NEXT POINT: ~p ~n", [Radius]));

get_info([], _) ->
    [].

