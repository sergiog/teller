-- Copyright © 2012-2013 Ángel Herranz
-- All rights reserved

--  Internal units per dimention:False 
--  - Distance: meters
--  - Latitude/longitude: degrees
--  - Angles: radians

import Text.CSV (Record, parseCSVFromFile)
import System.Locale (defaultTimeLocale)
import Data.Time.Format (parseTime, readTime)
import Data.Time.Clock (UTCTime, utctDayTime, diffUTCTime, NominalDiffTime, getCurrentTime)

-- Constants and auxiliary functions
earthRadius :: Double
earthRadius = 6372797.560856

radPerDeg :: Double
radPerDeg = 0.017453292519943295769236907684886;  

radFromDeg :: Double -> Double
radFromDeg d = radPerDeg * d

degFromRad :: Double -> Double
degFromRad r = r / radPerDeg

-- GPS positions (same as CSV file information from BT747)
data GPSPos = GPSPos
    {
      ind :: Int,
      utc :: UTCTime,
      lat :: Double,
      lon :: Double,
      hei :: Double,
      spe :: Double,
      hea :: Double,
      hdop :: Double,
      dist :: Double
    }

instance Show GPSPos where
    show gpsp | isNullGPSPos gpsp = "nullGPSPos"
              | otherwise = ""
                            ++ show (ind gpsp) ++ ","
                            ++ show (utc gpsp) ++ ","
                            -- ++ show (lat gpsp) ++ ","
                            -- ++ show (lon gpsp) ++ ","
                            -- ++ show (hei gpsp) ++ ","
                            ++ show (spe gpsp) ++ ","
                            ++ show (hea gpsp) ++ ","
                            -- ++ show (hdop gpsp) ++ ","
                            ++ show (dist gpsp) ++ ","

-- Just to indicate end of gpspos read from CSV files
nullGPSPos :: GPSPos
nullGPSPos =
    GPSPos
    {
      ind = -1,
      utc = error "NoTime",
      lat = 0,
      lon = 0,
      hei = 0,
      spe = 0,
      hea = 0,
      hdop = 0,
      dist = 0
    }

isNullGPSPos :: GPSPos -> Bool
isNullGPSPos gpsp = ind gpsp < 0 

-- Information compiled with a path of GPSPos values
data PathInfo = PathInfo
    {
      path :: [GPSPos],  -- The path (in reverse order)
      pathDist :: Double, -- Distance between p1 and pn (m)
      pathBear :: Double, -- Bearing relative to North=0 (deg) from p1 to pn
      pathSpeed :: Double  -- Speed (km/h)
    }

instance Show PathInfo where
    show pi = ""
              ++ (if length (path pi) > 0 then show (last (path pi)) else "")
              ++ show (pathSpeed pi) ++ ","
              ++ show (pathBear pi) ++ ","
              ++ show (pathDist pi) ++ ","
              ++ show (pathRadius pi) ++ ","

-- Distance
-- http://en.wikipedia.org/wiki/Haversine_formula
-- http://www.codecodex.com/wiki/Calculate_Distance_Between_Two_Points_on_a_Globe
haverSine :: GPSPos -> GPSPos -> Double
haverSine from to =
    let latArc = radFromDeg (lat from - lat to)
        lonArc = radFromDeg (lon from - lon to)
        latH = (sin (latArc / 2)) ** 2
        lonH = (sin (lonArc / 2)) ** 2
        tmp = cos (radFromDeg (lat from)) * cos (radFromDeg (lat to))
        radius = earthRadius + (hei from + hei to) / 2
    in radius * 2 * asin (sqrt (latH + tmp * lonH))

distance :: GPSPos -> GPSPos -> Double
distance = haverSine

-- Bearing (Nort=0)
-- http://mathforum.org/library/drmath/view/55417.html
-- http://www.movable-type.co.uk/scripts/latlong.html
-- http://www.ig.utexas.edu/outreach/googleearth/latlong.html
bearing :: GPSPos -> GPSPos -> Double
bearing from to =
    let latFrom = radFromDeg (lat from)
        latTo = radFromDeg (lat to)
        lonFrom = radFromDeg (lon from)
        lonTo = radFromDeg (lon to)
        dLon = lonTo - lonFrom --radFromDeg (lon to - lon from)
        d = distance from to
    in atan2 (sin dLon * cos latTo)
             (cos latFrom * sin latTo - sin latFrom * cos latTo * cos dLon)

-- 
readBT747CSV :: FilePath -> IO [GPSPos]
readBT747CSV file =
    do x <- parseCSVFromFile file
       case x of
         Left _parseError ->
             return []
         Right (_header:records) ->
             return $ map gpsPosFromRecord records

-- http://en.wikipedia.org/wiki/Dilution_of_precision_(GPS)
-- DOP	Rating	Description
-- 1	Ideal	This is the highest possible confidence level to be used for applications demanding the highest possible precision at all times.
-- 1-2	Excellent	At this confidence level, positional measurements are considered accurate enough to meet all but the most sensitive applications.
-- 2-5	Good	Represents a level that marks the minimum appropriate for making business decisions. Positional measurements could be used to make reliable in-route navigation suggestions to the user.
-- 5-10	Moderate	Positional measurements could be used for calculations, but the fix quality could still be improved. A more open view of the sky is recommended.
-- 10-20	Fair	Represents a low confidence level. Positional measurements should be discarded or used only to indicate a very rough estimate of the current location.
-- >20	Poor	At this level, measurements are inaccurate by as much as 300 meters with a 6 meter accurate device (50 DOP × 6 meters) and should be discarded.
gpsPosFromRecord :: Record -> GPSPos
gpsPosFromRecord (index:_rcr:date:time:_valid:latitude:_ns:longitude:_ew:height_m:speed_km_h:heading:hdop:distance_m:_) =
    let ind = read index
        t = readTime defaultTimeLocale "%Y/%m/%d %H:%M:%S%Q" (date ++ time)
        lat = read latitude
        lon = read longitude
        hei = read height_m
        spe = read speed_km_h
        hea = read heading
        hdo = read hdop
        dist = read distance_m
    in GPSPos ind t lat lon hei spe hea hdo dist
gpsPosFromRecord _ = nullGPSPos

mapBy :: Int -> ([a] -> b) -> [a] -> [b]
mapBy n f xs | length xs < n = []
             | otherwise = f (take n xs) : mapBy n f (tail xs)

pathInfo :: [GPSPos] -> PathInfo
pathInfo = foldl addGPSPos (PathInfo [] 0 0 0)

addGPSPos :: PathInfo -> GPSPos -> PathInfo
addGPSPos pi gpsp | isNullGPSPos gpsp =
    pi
addGPSPos (PathInfo [] _ _ _) gpsp =
    PathInfo [gpsp] 0 0 0
addGPSPos (PathInfo gpsps d _b _s) gpsp =
    let gpsp1 = last gpsps
        gpspn = head gpsps
        n = length gpsps
        gpspm = gpsps !! (n `div` 2)
        d' = d + distance gpspn gpsp
        b' = degFromRad (bearing gpsp1 gpsp)
        t1 = utc gpsp1
        t = utc gpsp
        dT = realToFrac $ diffUTCTime t t1
        s' = 3600 * d' / dT / 1000
    in PathInfo (gpsp:gpsps) d' b' s'

pathRadius :: PathInfo -> Double
pathRadius pi =
    let gpsps = path pi
        n = length gpsps
    in if n < 3
       then 0
       else radius (last gpsps) (gpsps !! (n `div` 2)) (head gpsps)

radius :: GPSPos -> GPSPos -> GPSPos -> Double
radius p1 o p2 =
    let p1_r = distance o p1
        p1_ae = degFromRad (bearing o p1)
        p2_r = distance o p2
        p2_ae = degFromRad (bearing o p2)
    in radius2 p1_r p1_ae p2_r p2_ae

radius2 :: Double -> Double -> Double -> Double -> Double
radius2 p1_r p1_ae p2_r p2_ae =
    -- O	Punto origen de los dos segmentos (origen de coordenadas)
    -- P1	Punto final del primer segmento
    -- P2	Punto final del segundo segmento
    -- V1	Punto en la bisectriz del primer segmento
    -- V2	Punto en la bisectriz del segundo segmento
    -- R	Coordenada polar del radio
    -- AE	Ángulo en grados tal y como lo devuelve google earth
    -- A	Coordenada polar del ángulo en radianes)
    -- X	Abscisa
    -- Y	Ordenada
    -- M	Pendiente de la recta
    -- B	Corte de la recta con el eje de ordenadas
    -- b1	Bisectriz del primer segmento
    -- b2	Bisectriz del segundo segmento
    -- C	Centro de la circunferencia
    let p1_a = radFromDeg (90 - p1_ae)
        p2_a = radFromDeg (90 - p2_ae)
        v1_r = p1_r / 2
        v1_a = p1_a
        v2_r = p2_r / 2
        v2_a = p2_a
        p1_x = p1_r * cos p1_a
        p1_y = p1_r * sin p1_a
        p2_x = p2_r * cos p2_a
        p2_y = p2_r * sin p2_a
        v1_x = v1_r * cos v1_a
        v1_y = v1_r * sin v1_a
        v2_x = v2_r * cos v2_a
        v2_y = v2_r * sin v2_a
        b1_m = tan (pi / 2 + v1_a)
        b1_b = v1_y - b1_m * v1_x
        b2_m = tan (pi / 2 + v2_a)
        b2_b = v2_y - b2_m * v2_x
        c_x = (b2_b - b1_b) / (b1_m - b2_m)
        c_y = b1_m * c_x + b1_b
        c_r = sqrt (c_x ^ 2 + c_y ^ 2)
    in c_r

-- Smoothing
-- http://en.wikipedia.org/wiki/Exponential_smoothing
-- http://en.wikipedia.org/wiki/Kalman_filter
-- http://www.statsoft.com/textbook/time-series-analysis/
-- http://en.wikipedia.org/wiki/Smoothing

-- inputfile = "salida-20120421.csv"
-- inputfile = "formulas-curva-501-gsx.csv"
-- inputfile = "GPSDATA-20120910_2208.csv"
-- inputfile = "GPSDATA-20120910_1846.csv"
inputfile = "k.csv"

main :: IO ()
main =
    do gpsps <- readBT747CSV inputfile
       let pis = mapBy 3 pathInfo gpsps
       putStrLn "INDEX,UTC,SPEED(km/h),HEADING,DISTANCE(m),CSPEED,CHEADING,CDISTANCE,CRADIUS,"
       mapM_ print pis
       -- let pi = head pis
       -- let p2 = path pi !! 0
       -- let o = path pi !! 1
       -- let p1 = path pi !! 2
       -- print pi
       -- print p1
       -- print o
       -- print p2
       -- print $ distance o p1
       -- print $ degFromRad (bearing o p1)
       -- print $ distance o p2
       -- print $ degFromRad (bearing o p2)
       -- print $ radius p1 o p2

-- GPS info:
-- http://www.elgps.com/documentos.html
-- http://www.souterrain.biz/A%20short%20GUIDE%20TO%20GPS3.htm
-- http://www.gpsmap.net/DefiningPoints.html
