%% Copyright © 2013 Sergio Gil Luque, Ángel Herranz
%% All rights reserved

%% @author Sergio Gil Luque
%% @version 0.30
%% @title Telemetria con GPS - Modulo classifier
%% @doc Classifies gps points into sections

-module(classifier).

 
%% @headerfile records_lib.hrl
-include("../include/records_lib.hrl").

-export([begin_classification/1]).


%% @doc First argument is the list containing all the gps points with all the calculations made by module mathsGPS. Second argument is the radius that divides points into straights or curves


begin_classification([_, _| T]) ->
    classify([],T).



classify(L,[P1|T]) when is_record (P1, gps_operations)  ->
    Head_diff = P1#gps_operations.headingDiff, 

    if
	Head_diff >= 1.0 ->
	    {C,R} = curve_bucle(["CURVE",P1],T);

	Head_diff < 1.0 ->
	    {C,R} = straight_bucle(["STRAIGHT",P1],T)
	     
    end,

    classify(lists:append(L,C),R);

classify(L,[]) ->
L.



curve_bucle(Res, []) ->
{[Res],[]};

curve_bucle(Res, [H|T]) when is_record (H, gps_operations) ->
    %P_radius = H#gps_operations.radius,
    Head_diff = H#gps_operations.headingDiff,

    if
	Head_diff >= 1.0 ->
	%P_radius < Radius ->
	    curve_bucle(lists:append(Res,[H]),T);

	Head_diff < 1.0 ->
	%P_radius >= Radius ->
	    {[Res],[H|T]}
    end.



straight_bucle (Res, [])->
{[Res],[]};

straight_bucle (Res, [H|T]) when is_record (H, gps_operations) ->
    %P_radius = H#gps_operations.radius,
    Head_diff = H#gps_operations.headingDiff,
    if
	Head_diff < 1.0 ->
	%P_radius >= Radius ->
	    straight_bucle(lists:append(Res,[H]) ,T);

	Head_diff >= 1.0 ->
	%P_radius < Radius ->
	    {[Res], [H|T]}
    end.
