%% Copyright © 2013 Sergio Gil Luque, Ángel Herranz
%% All rights reserved

%% @author Sergio Gil
%% @version 0.52
%% @title Telemetria con GPS - Modulo csvParser

%% @doc This is a csv parser in Erlang. The argument required for this module is a csv file. It will be converted into a list containing the separated values from the original file. For each line, a new list will be created.

-module(csvParser).

%% @headerfile records_lib.hrl
-include("../include/records_lib.hrl").

-export([parse/1]).


%% @type Gps_point = #gps_point{index = integer(),rcr=,date=,time=,valid=,latitude=float(),n_s= N | S,longitude = float(),e_w = E|W,height= float(),speed = float(),heading,hdop,distance = float()}


%% @spec list_to_record (List) -> Gps_point
%%	List = [string()]



%% @spec parse ([string()]) -> {error, Reason} | List
%%	List = [Gps_point]
parse(File) when is_list (File) ->
	{ok, F} = file:open(File, [read, raw]),
	parse(F, file:read_line(F), []).


%% @doc If it reaches the end of the file, transform it into records and close file
%% @spec parse (Handle, eof, List) -> Result
%%	Handle = file_handle()
%%	List = [string()]
%%	Result = [Gps_point]
parse(F, eof, Done) ->
        file:close(F),
        J = lists:reverse(Done),
        L = transform_list (J),
        L;



%% @spec parse (Handle, {ok, Line}, List) -> Result
%%	Handle = file_handle()
%%	Line = [string()]
%%	List = [string()]
%%	Result = [Gps_point]
parse(F, {ok, Line}, Done) ->
	parse(F, file:read_line(F), [parse_line(Line)|Done]).


list_to_record([Index, RCR, Date, Time, Valid, Latitude, N_S, Longitude, E_W, Height, Speed, Heading, Hdop, Distance]) -> 
        #gps_point{
		index = strToInt(Index), 
		rcr = RCR, 
		date = parse_date(Date), 
		time = parse_time(Time), 
		valid = Valid, 
		latitude = strToFloat(Latitude), 
		n_s = N_S, 
		longitude = strToFloat (Longitude), 
		e_w = E_W, 
		height = strToFloat(Height), 
		speed = strToFloat(Speed), 
		heading = strToFloat(Heading), 
		hdop = strToFloat(Hdop), 
		distance = strToFloat(Distance)
	       }.



%% @spec list_to_record_raw (List) -> [Gps_point]
%%	List = [string()]
list_to_record_raw([Index, RCR, Date, Time, Valid, Latitude, N_S, Longitude, E_W, Height, Speed, Heading, Hdop, Distance]) ->        
        #gps_point{
		    index = Index, 
		    rcr = RCR, 
		    date = Date, 
		    time = Time, 
		    valid = Valid, 
		    latitude = Latitude, 
		    n_s = N_S, 
		    longitude = Longitude, 
		    e_w = E_W, 
		    height = Height, 
		    speed = Speed, 
		    heading = Heading, 
		    hdop = Hdop, 
		    distance = Distance
		  }.



%% @spec strToInt( string() ) -> integer()       
strToInt(Value) -> 
	{X,_} = string:to_integer(Value),
	X. 



%% @spec strToFloat( string() ) -> float()
strToFloat(Value) ->
	{X,_} = string:to_float(Value),
	X.



%% @spec transform_list (List) -> (Result)
%%	List = [[ string()]]
%%	Result = [[string()]]
transform_list ([H|T]) -> 
	[list_to_record_raw(H)|[list_to_record(X) || X <- T]].


%% @spec parse_line ([string()]) -> [string()]
parse_line(Line) -> 
	parse_line(Line, []).



%% @spec parse_line ([], [_ | [string()]]) -> [string()]
% Last element of every list (Foo, which is the new line atom) will not be included after the file is parsed
parse_line([], [_ | Fields]) -> 
	lists:reverse(Fields);



%% @spec parse_line (Del, [string()]) -> [string()]
%%	Del = "," ++ [string()]
parse_line("," ++ Line, Fields) -> 
	parse_field(Line, Fields);



%% @spec parse_line ([string()], [string()]) -> [string()]
parse_line(Line, Fields) -> 
	parse_field(Line, Fields).



%% @spec parse_field (Sl_Del, [string()]) -> string()
%%	Sl_Del = '\' ++ string()
parse_field("\"" ++ Line, Fields) -> 
	parse_field_q(Line, [], Fields);



%% @spec parse_field ([string()], [string()]) -> string()
parse_field(Line, Fields) -> 
	parse_field(Line, [], Fields).



%% @spec parse_field(Com_Del,[string()],string()) -> string()
%%	Com_Del = ',' ++ string()
parse_field("," ++ _ = Line, Buf, Fields) -> 
	parse_line(Line, [lists:reverse(Buf)|Fields]);



%% @spec parse_field ([string()],[string()], string()) -> string()
parse_field([C|Line], Buf, Fields) -> 
	parse_field(Line, [C|Buf], Fields);



%% @spec parse_field ([], [string()], string()) -> string()
parse_field([], Buf, Fields) -> 
	parse_line([], [lists:reverse(Buf)|Fields]).


%% @spec parse_field_q (Double_Slash_Del, [string()], string) -> string()
%%      Double_Slash_Del = '\"\"' ++ [string()]
parse_field_q("\"\"" ++ Line, Buf, Fields) -> 
	parse_field_q(Line, [$"|Buf], Fields);



%% @spec parse_field_q (Slash_Del, [string()], string) -> string()
%%	Slash_Del = '\"' ++ [string()]
parse_field_q("\"" ++ Line, Buf, Fields) -> 
	parse_line(Line, [lists:reverse(Buf)|Fields]);



%% @spec parse_field_q ([string()], [string()], string()) -> string()
parse_field_q([C|Line], Buf, Fields) -> 
	parse_field_q(Line, [C|Buf], Fields).


%% @spec parse_date (string()) -> {year(),month(),day()}
parse_date([Y1,Y2,Y3,Y4,_,M1,M2,_,D1,D2]) ->
    {erlang:list_to_integer([Y1,Y2,Y3,Y4]),
     erlang:list_to_integer([M1,M2]),
     erlang:list_to_integer([D1,D2])
    }.


%% @spec parse_time (string()) -> {hours(), minutes(), seconds(), Milseconds}
%%	Miliseconds = [integer()]
parse_time([H1,H2,_,M1,M2,_,S1,S2,_,Mm1,Mm2,Mm3]) ->
    {erlang:list_to_integer([H1,H2]),
     erlang:list_to_integer([M1,M2]),
     erlang:list_to_integer([S1,S2]),
     erlang:list_to_integer([Mm1,Mm2,Mm3])
    }.
