%% Copyright © 2013 Sergio Gil Luque, Ángel Herranz
%% All rights reserved

%% @doc Telemetry vectors, segments, stretches and sections. A
%% telemetry vector contains the trackpoints used to derive telemetry
%% parameters. A section represents a uniform part of a track (several
%% km, around 5, 10, 15) that is divided in stretches. A stretch
%% represents a shorter part of a track (several meters, around 50,
%% 100, 200, 300 meters) that can be classified as "straight",
%% "curve", "chicane", etc. A stretch is divided in segments. A
%% segment is a part of a track (a few meters, around 10, 25, 50, 100)
%% that tries to identify relevant rider actions such as breaking,
%% accerelating, countersteering, lean angle, etc.
-module(telvecs).

-include("telvec_rec.hrl").

%-type telvec() :: #telvec{}.
%% TODO: need to improve cases
%-type segmenttype() :: {'breaking', 'accelerating', 'leaning' }.
%-type segment() :: { segtype(), [telvec()] }.
%% TODO: need to improve cases
%-type stretchtype() :: {'curve', 'straight', 'chicane'}.
%-type stretch() :: { stretchtype(), [segment()] }.
%-type section() :: [stretch()].
