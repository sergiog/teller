%% Copyright © 2013 Sergio Gil Luque, Ángel Herranz
%% All rights reserved

%% @author Sergio Gil Luque
%% @version 0.40
%% @title Telemetria con GPS - Modulo htmlGenerator
%% @doc Creates html file for visualization


-module(htmlGenerator).


%% @headerfile records_lib.hrl
-include("../include/records_lib.hrl").

-export ([generate/4]).

generate([_|T], Path, File_name, URL) ->
    
    %{ok, Html} = file:open("/home/sergiog/public_html/telemetry/map1.html", [write]), 
    {ok, Html} = file:open(Path ++ File_name ++ ".html", [write]), 

    %{ok, Html} = file:open(Path ++ "map" ++ Count ++ ".html", [write]), 
    %{ok, Html} = file:open(Html_path0, [write]), 

    ok = file:write(Html, io_lib:fwrite(
"<!DOCTYPE html>
<html>
  <head>
    <meta name=\"viewport\" content=\"initial-scale=1.0, user-scalable=no\" />
    <style type=\"text/css\">
      html { height: 100% }
      body { height: 100%; margin: 0; padding: 0 }
      #map-canvas { height: 100% }
    </style>
    <script type=\"text/javascript\"
      src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyDS-QIWZYYBznLVzHXAGswbuqOWXCO0jd0&sensor=true\">
    </script>
    <script type=\"text/javascript\">

    var infoCoordinates = ["
			    ,[])),

  %var kmlFile = 'http://babel.upm.es/%7esergiog/telemetry/cta1.kml?'+(new Date()).getTime();
    
    bucle_info_first(T, Html),

    ok = file:write(Html, io_lib:fwrite("
     function initialize() {
          var mapOptions = {
          center: new google.maps.LatLng(-3.582929,40.613903),
          zoom: 8,
          mapTypeId: google.maps.MapTypeId.ROADMAP
          };

      var map = new google.maps.Map(document.getElementById(\"map-canvas\"), mapOptions);

      var kmlFile = '~ts.kml?'+(new Date()).getTime();
      var kmlLayer = new google.maps.KmlLayer(kmlFile);
      kmlLayer.setMap(map);
    

      }

      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
  </head>
  <body>
    <div id=\"map-canvas\"/>
</body>
</html>"
					,[URL ++ File_name])).



bucle_info_first( [H|T], Html) ->
   Lat = H#gps_operations.lat,
   Long =  H#gps_operations.long,
   Speed = H#gps_operations.speedGPS,
   Acc = H#gps_operations.acceleration,
   Index = H#gps_operations.index,

   ok = file:write(Html, io_lib:fwrite("
   [~f,~f,~f,~p,~p]",[Lat,Long,Speed,Acc,Index])),
   bucle_info_second(T, Html).

bucle_info_second([H|T], Html) ->
   Lat = H#gps_operations.lat,
   Long =  H#gps_operations.long,
   Speed = H#gps_operations.speedGPS,
   Acc = H#gps_operations.acceleration,
   Atan = H#gps_operations.a_tan,
   Arad = H#gps_operations.a_rad,
   Index = H#gps_operations.index,

   ok = file:write(Html, io_lib:fwrite(",
   [~f,~f,~f,~p,~p,~p,~p]",[Lat,Long,Speed,Acc,Index,Atan,Arad])),
   bucle_info_second(T, Html);

bucle_info_second ([], Html)->
    ok = file:write(Html, io_lib:fwrite("];",[])).

