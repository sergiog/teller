%% Copyright © 2013 Sergio Gil Luque, Ángel Herranz
%% All rights reserved

%% @author Sergio Gil Luque
%% @version 0.40
%% @title Telemetria con GPS - Modulo initializer
%% @doc Start of the program, which calls the other modules


-module(initializer).

%% @headerfile records_lib.hrl
-include("../include/records_lib.hrl").

-export([main/4]).

%% @doc main function of the program. First argument must be the path of a csv file which contains gps points from BT747 program. Second argument is a test: The radius of the curve

%% @spec main (string(), integer()) -> {error, string()} | [string()]
main (Input_file, File_name, Output_dir, URL) ->

    io:format("~n~n   ********************~n"),
    io:format("    TELEMETRY PROJECT ~n"),
    io:format("          v0.50a       ~n"),
    io:format("   ********************~n~n"),

    io:format("~n+INPUT FILE PATH: '~ts'~n", [Input_file]),


    {ok, Tlog} = file:open("../test/tech_log", [write]), 
    {ok, Mlog} = file:open("../test/maths_log", [write]), 
    {ok, Ulog} = file:open("../test/user_log", [write]), 

    %%erlang:group_leader(Log, self()), %% output redirected to file "log"
    io:format("~n+PARSING FILE..."),
    Parsed_file = csvParser:parse(Input_file),
    io:format("\tDONE~n"),

    io:format("~n+MAKING CALCULATIONS..."),
    L = mathsGPS:begin_calculations(Parsed_file),
    io:format("\tDONE~n"),

    io:format("~n+CLASSIFYING TRAMS..."),
    Res = classifier:begin_classification(L),
    io:format("\tDONE~n"),

    io:format("~n+GENERATING LOG FILES...~n"),
    io:format("     |- FILE 'maths_log'..."),
    ok = file:write(Mlog, io_lib:fwrite("~p.\n", [L])),
    io:format("\tDONE~n"),
    io:format("     |- FILE 'tech_log'..."),
    ok = file:write(Tlog, io_lib:fwrite("~p.\n", [Res])),
    io:format("\tDONE~n"),
    io:format("     |- FILE 'user_log'..."),
    userLogMsg:begin_recopilation(L,Ulog),
    io:format("\tDONE~n"), 
    io:format("     \\+ PATH FOR LOG FILES IS '../test' (FROM CURRENT DIRECTORY)~n"),

    io:format("~n+GENERATING VISUALIZATION FILES...~n",[]),
    io:format("     |- KML FILE..."),
    klmGenerator:generate(Res, L, Output_dir, File_name),
    io:format("\t\tDONE~n"),
    io:format("     |- HTML FILE..."),
    htmlGenerator:generate(L,Output_dir, File_name, URL),
    io:format("\t\tDONE~n"),

    io:format("~n**SUCESS**~n").

