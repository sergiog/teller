%% Copyright © 2013 Sergio Gil Luque, Ángel Herranz
%% All rights reserved

%% TODO: to add a step to clean clear wrong or undesired data.

%% TODO: to add functions to split tracks up in stages and sections

-module(trackps).

-export([splitup/1]).

-include("trackp_rec.hrl").

% Trackpoints: per registry information logged by the GPS data logger
-type trackp() :: #trackp{}.
% Tracks: just a list of trackpoints (directly downloaded from the GPS
% data logger)
-type track() :: [trackp()].
%% % Stages: tracks are initially split up in order to separate tracks
%% % because of long pauses, long distances, device switche on/off, etc.
%% -type stage() :: [trackp()].
%% % Sections: a stage can be split up in order to detect irrelevant
%% % parts such as very slow parts
%% -type section() :: [trackp()].

%% @doc Splits a track in <em>continious</em> tracks.
-spec splitup(track()) -> [track()].
splitup(T) ->
    {T1,Rest} = continuous(T),
    [T1 | splitup(Rest)].

-spec continuous(track()) -> {track(), track()}.
continuous([]) ->
    {[], []};
continuous([P1]) ->
    {[P1], []};
continuous([P1,P2|Rest]) ->
    case break(P1,P2) of
        true ->
            {[P1],[P2|Rest]};
        false -> 
            {T1,T2} = continuous([P2|Rest]),
            {[P1|T1],T2}
    end.

%% TODO: to implement the split criteria (long time, long distance, ...)
-spec break(trackp(),trackp()) -> boolean().
break(_P1,_P2) ->
    false.
