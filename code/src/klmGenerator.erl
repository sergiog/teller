%% Copyright © 2013 Sergio Gil Luque, Ángel Herranz
%% All rights reserved

%% @author Sergio Gil Luque
%% @version 0.40
%% @title Telemetria con GPS - Modulo klmGenerator
%% @doc Creates klm file for visualization


-module(klmGenerator).

%% @headerfile records_lib.hrl
-include("../include/records_lib.hrl").

-export([generate/4]).

generate (List, [_|T], Path, File_name) ->

    %Path = "/home/sergiog/public_html/telemetry/",
    %io:format("~n+++++++ Path is: ~p~n",[Path ++ "cta1.kml"]),
    %{ok, Klm} = file:open("/home/sergiog/public_html/telemetry/cta1.kml", [write]), 
    {ok, Klm} = file:open(Path ++ File_name ++ ".kml", [write]), 

    %{ok, Klm} = file:open(Path ++ "cta" ++ Count ++ ".kml", [write]), 
    %{ok, Klm} = file:open(Klm_path0, [write]), 

    ok = file:write(Klm, io_lib:fwrite(
"<?xml version=\"1.0\" encoding=\"UTF-8\"?>~n
<kml xmlns=\"http://earth.google.com/kml/2.1\">

  <Document>
    <name>Test-001</name>
    <description>Telemetry Project First Test</description>

    <Style id=\"blueLine\">
      <LineStyle>
        <color>ffff0000</color>
        <width>4</width>
      </LineStyle>
    </Style>

    <Style id=\"redLine\">
      <LineStyle>
        <color>ff0000ff</color>
        <width>4</width>
      </LineStyle>
    </Style>~n" , [])),

 
    process_list(List, Klm),
    process_points(T,Klm),
    close_file(Klm).


close_file (Klm) ->
      ok = file:write(Klm, io_lib:fwrite(" 
  </Document>
</kml>", [])).

process_points([],Klm) ->
[];

process_points([H|T], Klm) ->
   Lat = H#gps_operations.lat,
   Long =  H#gps_operations.long,
   Speed = H#gps_operations.speedGPS,
   Acc = H#gps_operations.acceleration,
   Atan = H#gps_operations.a_tan,
   Arad = H#gps_operations.a_rad,
   Index = H#gps_operations.index,
   ok = file:write(Klm, io_lib:fwrite("
<Placemark> 
 <name> New point</name> 
 <description>
Point ~p
Speed: ~f km\h
Acceleration (total): 0 m/s^2
     + Tangential: 0 m/s^2
     + Radial: 0 m/s^2
</description>
 <Point>
  <coordinates>
   ~f, ~f, 0.
  </coordinates>
 </Point> 
</Placemark>~n",[Index,Speed,Long, Lat])),

process_points_2(T,Klm).

process_points_2([],Klm) ->
[];

process_points_2([H|T], Klm) ->
   Lat = H#gps_operations.lat,
   Long =  H#gps_operations.long,
   Speed = H#gps_operations.speedGPS,
   Acc = H#gps_operations.acceleration,
   Atan = H#gps_operations.a_tan,
   Arad = H#gps_operations.a_rad,
   Index = H#gps_operations.index,
   ok = file:write(Klm, io_lib:fwrite("
<Placemark> 
 <name>Trackpoint ~p</name> 
 <description>
Speed: ~f km/h <br/>
Acceleration (total): ~f m/s<sup>2</sup> <br/>
     + Tangential: ~f m/s<sup>2</sup> <br/>
     + Radial: ~f m/s<sup>2</sup> <br/>
</description>
 <Point>
  <coordinates>
   ~f, ~f, 0.
  </coordinates>
 </Point> 
</Placemark>~n",[Index,Speed,Acc,Atan,Arad  ,Long, Lat])),

process_points_2(T,Klm).

process_list([H|T], Klm) ->

    Last = lists:last(H), 
    case H of
	["STRAIGHT" | Points ] ->
	    %[Last|_] = list:reverse(Points),
	    draw_straight (Points, Klm);

	[ "CURVE" | Points ] ->
	    %[Last|_] = list:reverse(Points),
	    draw_curve (Points, Klm);

	_ -> 'SOMETHING WENT WRONG'

    end,
    process_list_2 (T,Last,Klm);

process_list([],_) ->
    [].

process_list_2([H|T],Last, Klm) ->
    New_last = lists:last(H), % Take last point of actual tram to link it with the following tram (prevent spaces between trams)

    case H of
	["STRAIGHT" | Points ] ->
	    United_points = lists:append([Last],Points), %last point appended with next tram
	    draw_straight (United_points, Klm);

	[ "CURVE" | Points ] ->
	    United_points = lists:append([Last],Points),  %last point appended with next tram
	    draw_curve (United_points, Klm);

	_ -> 'SOMETHING WENT WRONG'

    end,
    process_list_2 (T,New_last,Klm);

process_list_2 ([],_,_) ->
    [].

draw_straight (L, Klm) ->
    ok = file:write(Klm, io_lib:fwrite(" 
    <Placemark>
      <styleUrl>#blueLine</styleUrl>
      <LineString>
        <altitudeMode>relative</altitudeMode>
        <coordinates>~n",[])),


    Last = str_bucle(L,Klm),
    

    ok = file:write(Klm, io_lib:fwrite(
"       </coordinates>
      </LineString>
    </Placemark>",[])),
    Last.

str_bucle([H|T], Klm) ->
    ok = file:write(Klm, io_lib:fwrite("~f,", [H#gps_operations.long])),
    ok = file:write(Klm, io_lib:fwrite("~f,", [H#gps_operations.lat])),
    ok = file:write(Klm, io_lib:fwrite("0~n", [])),
    str_bucle(T, Klm);

str_bucle([],_) ->
    [].


draw_curve (L, Klm) ->
        ok = file:write(Klm, io_lib:fwrite("~n 
    <Placemark>
      <styleUrl>#redLine</styleUrl>
      <LineString>
        <altitudeMode>relative</altitudeMode>
        <coordinates>~n",[])),

    crv_bucle(L,Klm),
    
    ok = file:write(Klm, io_lib:fwrite(
"       </coordinates>
      </LineString>
    </Placemark>~n",[])).

crv_bucle([H|T], Klm) ->
    ok = file:write(Klm, io_lib:fwrite("~f,", [H#gps_operations.long])),
    ok = file:write(Klm, io_lib:fwrite("~f,", [H#gps_operations.lat])),
    ok = file:write(Klm, io_lib:fwrite("0~n", [])),
    crv_bucle(T, Klm);

crv_bucle([],_) ->
    [].
