%% Copyright © 2013 Sergio Gil Luque, Ángel Herranz
%% All rights reserved

%% @author Sergio Gil Luque
%% @author Ángel Herranz <aherranz@fi.upm.es>
%% @copyright 2013 Ángel Herranz, Sergio Gil Luque
%% @doc Mathematical operations with vectors 

-module(vectors).

-export([cart/2,polar/2]).
-export([a/1,r/1,x/1,y/1]).
-export([head_to_a/1, a_to_head/1]).
-export([dif/2, rot/2, sum/2]).

-type cart() :: {'cart', {float(), float()}}.
-type polar() :: {'polar', {float(), float()}}.
-type vector() :: cart() | polar().

%% @private
%% @doc Convert a vector to its Cartesian representation
-spec cart(vector()) -> cart().
cart({polar, {A,R}}) ->
    {cart,{R * math:cos(A), R * math:sin(A)}};
cart(V = {cart, _}) ->
    V.

%% @doc Vector constructor given its Cartesian coordinates
-spec cart(float(),float()) -> cart().
cart(X,Y) ->
    {cart, {X, Y}}.

%% @doc Abscissa (x-coordinate)
-spec x(vector()) -> float().
x(V) ->
    {cart, {X, _}} = cart(V),
    X.

%% @doc Ordinate (y-coordinate)
-spec y(vector()) -> float().
y(V) ->
    {cart, {_, Y}} = cart(V),
    Y.

%% @private
%% @doc Convert a vector to its Polar representation
-spec polar(vector()) -> polar().
polar({cart,{X,Y}}) ->
    R = math:sqrt(X*X + Y*Y),
    A = math:atan2(X,Y),
    {polar,{A, R}};

polar(V = {polar, _}) ->
    V.

%% @doc Vector constructor given its Polar coordinates
-spec polar(float(),float()) -> polar().
polar(A,R) ->
    {polar, {A,R}}.

%% @doc Angle (angular coordinate)
-spec a(vector()) -> float().
a(V) ->
    {polar, {A, _}} = polar(V),
    A.

%% @doc Radius (radial coordinate)
-spec r(vector()) -> float().
r(V) ->
    {polar, {_, R}} = polar(V),
    R.

%% TODO: to document
-spec head_to_a(float()) -> float().
head_to_a(H) ->
    A = to_rad(90 - H),
    if A < 0.0 -> 2*math:pi() + A;
       true -> A
    end.

%% TODO: to document
-spec a_to_head(float()) -> float().
a_to_head(A) ->
    H = to_deg(A),
    if H < 0.0 -> 360.0 + H;
       true -> H
    end.

%% TODO: to document
-spec to_rad(float()) -> float().
to_rad(D) ->
	(D * math:pi()) / 180.

%% TODO: to document
-spec to_deg(float()) -> float().
to_deg(R) ->
    (R * 180) / math:pi().

%% TODO: to document
-spec sum(vector(), vector()) -> vector().
sum(V1,V2) ->
    {cart,{X1,Y1}} = cart(V1),
    {cart,{X2,Y2}} = cart(V2),
    {cart, {X1 + X2, Y1 + Y2}}.

%% TODO: to document
-spec dif(vector(), vector()) -> vector().
dif(V1,V2) ->
    {cart,{X1,Y1}} = cart(V1),
    {cart,{X2,Y2}} = cart(V2),
    {cart, {X1 - X2, Y1 - Y2}}.

%% TODO: to document
-spec rot(vector(), float()) -> vector().
rot(V,DA) ->
    {polar,{A, R}} = polar(V),
    {polar, {A+DA, R}}.
