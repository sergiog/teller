********************
+++ USEFUL LINKS +++
********************


1) How to make a thesis introduction: http://www.ukdissertation.co.uk/Dissertation_Introduction.htm

2) Thesis and dissertations templates for latex: http://latexforhumans.wordpress.com/2011/03/10/thesis-templates-for-latex/

3) How to write your thesis: 
	
	3.1 http://www.ldeo.columbia.edu/~martins/sen_sem/thesis_org.html

	3.2 https://cs.uwaterloo.ca/~brecht/thesis-hints.html
